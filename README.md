# Profico Fullstack Task
***
## Deployed version:
https://ancient-temple-80282.herokuapp.com
***
## Usage:

1. Login:
	- for the first time usage, enter a **username: b**, **password: 123** and click _login_

2. Home:
	- to change your info, enter new values and click _save_
	- click _delete account_ to delete your account (not wise to do it before creating another admin)
	- navigate to _user list_ screen
	- click _logout_ to logout

3. User list:
	- to register new user click _add new user_, enter user's info, click _save_
	- click _edit_ or _delete_ to edit or delete any of the users listed in the table (editing other admins is not possible)
	
4. Swagger:
	- https://ancient-temple-80282.herokuapp.com/api-docs or localhost:5000/api-docs
***
## Setup locally:

1. Check do you have installed [Node.js](https://nodejs.org/en/) and package manager `npm`. If not, install.

	`node --version`
	
	`npm --version`
	
2. Clone the repository and navigate to the created directory:

	`git clone https://bvukas00@bitbucket.org/bvukas00/profico-app.git`
	
	`cd profico-app`
	
3. Rename .env.example to .env

4. In root directory install required modules using the command::

	`npm install`

5. In client directory install required modules:
   
	`cd client`

	`npm install`

6. Register or log in on [mongodb](https://cloud.mongodb.com/user#/atlas/login).

7. Create a new Cluster. In free version create a new project first [(photo)](https://imgur.com/vPrtSFS) :

8. Click on the connect and follow instructions [(photo)](https://imgur.com/jjOT4pc) : 
	1. add IP address [(photo)](https://imgur.com/QwaJ7Ps)
	2. create user
	3. choose "Connect Your Application" as a connection method [(photo)](https://imgur.com/mdRG4HU) 
	4. copy given connection string
	
9. In .env file paste given connection string as MONGO_URI

10. In the connection string, instead of <password> type password of the user that you created in step 8.2.

11. Register or log in at [openweathermap](https://openweathermap.org/current).

12. Copy your [API key](https://home.openweathermap.org/api_keys).

13. In .env file paste API key as WEATHER_API_KEY

14. In .env file enter the arbitrary string as JWT_SECRET

15. Run application from root directory using command:
	
	`npm run dev`
   
