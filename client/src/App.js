import React, { useEffect } from 'react';
import { Provider } from 'react-redux';

import axios from 'axios';

import setAuthToken from './utils/setAuthToken';
import store from './store';

import { Home, Login, UserList } from './components';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.scss';
import { AuthProvider } from './utils/Context/AuthContext/AuthContext';

const App = () => {
  axios.interceptors.response.use(
    response => response,
    error => {
      if (error.response.status === 401) {
        // logout
        sessionStorage.removeItem('token');
        sessionStorage.removeItem('admin');
      } else {
        return Promise.reject(error.response);
      }
    }
  );

  useEffect(() => {
    if (sessionStorage.token) {
      setAuthToken(sessionStorage.token);
    }
  }, []);

  return (
    <Provider store={store}>
      <Router>
        <AuthProvider>
          <Switch>
            <Route exact path="/" component={Login}></Route>
            <Route path="/login" component={Login}></Route>
            <Route path="/home" component={Home}></Route>
            <Route path="/user-list" component={UserList}></Route>
          </Switch>
        </AuthProvider>
      </Router>
    </Provider>
  );
};

export default App;
