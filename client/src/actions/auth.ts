import { IS_AUTHENTICATED, AppActions } from './types';
import { User } from '../models';

export const setIsAuthenticated = (isAuthenticated: boolean): AppActions => {
  return {
    type: IS_AUTHENTICATED,
    payload: isAuthenticated
  };
};
