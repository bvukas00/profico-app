import { SAVE_TOKEN, AppActions } from "./types";

export const saveToken = (token: string): AppActions => {
  return {
    type: SAVE_TOKEN,
    payload: token
  };
};
