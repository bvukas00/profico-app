import { User } from '../models';

export const SAVE_TOKEN = 'SAVE_TOKEN';
export const REMOVE_TOKEN = 'REMOVE_TOKEN';
export const LOAD_USER = 'LOAD_USER';
export const LOAD_USER_LIST = 'LOAD_USER_LIST';
export const UPDATE_USER = 'UPDATE_USER';
export const IS_AUTHENTICATED = 'IS_AUTHENTICATED';

export interface SaveTokenAction {
  type: typeof SAVE_TOKEN;
  payload: string;
}

export interface RemoveTokenAction {
  type: typeof REMOVE_TOKEN;
  payload: string;
}
export type TokenActionTypes = SaveTokenAction | RemoveTokenAction;

export interface LoadUserAction {
  type: typeof LOAD_USER;
  payload: User;
}

export interface LoadUserListAction {
  type: typeof LOAD_USER_LIST;
  payload: Array<User>;
}

export interface UpdateUserAction {
  type: typeof UPDATE_USER;
  payload: User;
}

export type UserActionTypes =
  | LoadUserAction
  | LoadUserListAction
  | UpdateUserAction;

export interface IsAuthenticatedAction {
  type: typeof IS_AUTHENTICATED;
  payload: boolean;
}

export type AuthActionTypes = IsAuthenticatedAction;

export type AppActions = TokenActionTypes | UserActionTypes | AuthActionTypes;
