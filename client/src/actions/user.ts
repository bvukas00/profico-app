import { LOAD_USER, LOAD_USER_LIST, UPDATE_USER, AppActions } from './types';
import { User } from '../models';

export const loadUser = (user: User): AppActions => {
  return {
    type: LOAD_USER,
    payload: user
  };
};

export const loadUserList = (userList: Array<User>): AppActions => {
  return {
    type: LOAD_USER_LIST,
    payload: userList
  };
};

export const updateUser = (user: User): AppActions => {
  return {
    type: UPDATE_USER,
    payload: user
  };
};
