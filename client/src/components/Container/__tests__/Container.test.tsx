import React from 'react';

import { shallow } from 'enzyme';
import Container, { ContainerProps } from '../';

describe('Container [component] ->', () => {
  let props: ContainerProps;

  beforeEach(() => {
    props = {
      children: 'Container'
    };
  });
  it('should render correctly', () => {
    const output = shallow(<Container />);
    expect(output.hasClass('Container')).toEqual(true);
    expect(output).toMatchSnapshot();
  });

  it('should render correctly with custom className', () => {
    props.className = 'testedContainter';
    const output = shallow(<Container {...props} />);

    expect(output.hasClass('testedContainter')).toEqual(true);
    expect(output).toMatchSnapshot();
  });
});
