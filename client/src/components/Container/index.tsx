import cn from 'classnames';
import React from 'react';
import styles from './styles.module.scss';

// moze za props primit className i sve propove koje html div el moze imat inace
export interface ContainerProps extends React.HTMLProps<HTMLDivElement> {
  className?: string;
}

const Container: React.FC<ContainerProps> = ({
  children,
  className,
  ...props
}) => (
  <div className={cn(styles.Container, className)} {...props}>
    {children}
  </div>
);

export default Container;
