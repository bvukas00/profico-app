import React from 'react';

import { shallow } from 'enzyme';
import Footer from '../';

describe('Footer [component] ->', () => {
  it('should render correctly', () => {
    const output = shallow(<Footer />);

    expect(output.hasClass('Footer')).toEqual(true);
    expect(output).toMatchSnapshot();
  });
});
