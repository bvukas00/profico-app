import React from 'react';
import Container from '../Container';
import styles from './styles.module.scss';

export default () => (
  <footer className={styles.Footer}>
    <Container>Footer</Container>
  </footer>
);
