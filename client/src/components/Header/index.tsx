import { Container } from '..';
import Logo from '../Logo';
import React from 'react';
import { Link } from 'react-router-dom';
import styles from './styles.module.scss';

const Header: React.FC = () => {
  return (
    <header className={styles.Header}>
      <Container className={styles.Container}>
        <div className={styles.Logo}>
          <Logo />
        </div>

        <div className={styles.Links}>
          {sessionStorage.getItem('admin') && (
            <div>
              <Link to="/user-list" className={styles.Link}>
                User List
              </Link>
            </div>
          )}

          <div>
            <Link
              to="/"
              className={styles.Link}
              onClick={() => sessionStorage.clear()}
            >
              Logout
            </Link>
          </div>
        </div>
      </Container>
    </header>
  );
};

export default Header;
