import React from 'react';
import { shallow } from 'enzyme';
import Home from '../';
import { BrowserRouter as Router } from 'react-router-dom';

describe('Home [component] ->', () => {
  it('should render correctly', () => {
    const output = shallow(
      <Router>
        <Home />
      </Router>
    );
    expect(output).toMatchSnapshot();
  });
});
