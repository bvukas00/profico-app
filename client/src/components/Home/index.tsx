import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Layout, UserInfo, Container, Spinner } from '..';
import { Redirect, useHistory } from 'react-router-dom';
import socketIOClient from 'socket.io-client';
import { User } from '../../models';
import { AppState } from '../../store';
import { connect } from 'react-redux';

import styles from './styles.module.scss';
import { UserReducerState } from '../../reducers/userReducer';
import { reduxForm, InjectedFormProps } from 'redux-form';

// export interface HomeProps {
//   myInfo: UserReducerState;
// }

// type HomeProps = InjectedFormProps<User>;

const Home: React.FC = () => {
  const [user, setUser] = useState<User>({
    name: '',
    username: '',
    email: '',
    city: '',
    role: ''
  });
  const [temp, setTemp] = useState<number>(null);
  const [fetched, setFetched] = useState<boolean>(false);
  const [error, setError] = useState<string>('');
  const [redArrow, showRedArrow] = useState<boolean>(false);
  const [greenArrow, showGreenArrow] = useState<boolean>(false);

  let history = useHistory();
  const { name, username, city, email, role } = user;

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await axios.get<User>('user/userinfo');
        let { name, username, city, email, role } = res.data;
        city = capitalizeFirstLetter(city);
        setUser({ name, username, city, email, role });

        const temperature = await axios.get(`city/temperature/${city}`);
        setTemp(temperature.data);
        setFetched(true);
      } catch (err) {
        console.log(err);
        setFetched(true);
      }
    };
    fetchData();
  }, []);

  useEffect(() => {
    // if inital temp is fetched
    if (temp) {
      // update temperature if changed
      const endpoint = `/?city=${city}&temp=${temp}`;
      const socket = socketIOClient(endpoint);
      socket.on('FromAPI', (data: number) => {
        setTemp(data);
        if (data > temp) {
          showRedArrow(false);
          showGreenArrow(true);
        } else if (data < temp) {
          showGreenArrow(false);
          showRedArrow(true);
        }
      });
    }
  }, [temp, city]);

  const capitalizeFirstLetter = (string: any) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  const clearStorageAndLogIn = () => {
    sessionStorage.clear();
    history.push('/');
  };

  const deleteAccount = async () => {
    if (
      window.confirm(
        'This action can not be undone! Do you want to delete your account?'
      )
    ) {
      try {
        const res = await axios.delete<User>('/user/delete');
        console.log(res.data);
        clearStorageAndLogIn();
      } catch (err) {
        console.log(err.response);
        setError('Error while deleting user');
      }
    } else {
      // Do nothing!
    }
  };

  const saveChanges = async (updatedUser: User) => {
    const body = { ...updatedUser, oldUsername: username, oldCity: city };
    try {
      const res = await axios.post<User>('/user/update', body);
      console.log(res);
      alert('Changes saved succsessfuly. Please, log in.');
      clearStorageAndLogIn();
    } catch (err) {
      console.log(err);
      if (err.response.data.errors) setError(err.response.data.errors[0].msg);
    }
  };

  if (!sessionStorage.getItem('token')) {
    // koristi self closing tags di mozes
    return <Redirect to="/" />;
  }

  return (
    <Layout>
      {fetched ? (
        <Container className={styles.Container}>
          <div className={styles.UserInfo}>
            <h1>Welcome {name}!</h1>
            {error && <p className={'ErrorInput'}>{error}</p>}
            <UserInfo
              name={name}
              username={username}
              city={city}
              email={email}
              role={role}
              handleDelete={deleteAccount}
              deleteValue={'Delete account'}
              onSubmit={saveChanges}
              modal={false}
              parent={'Home'}
            ></UserInfo>
          </div>
          <div>
            <div>
              {temp ? (
                <>
                  <h2 className={styles.Temp}>
                    Current temperature in {city} is {temp} °C.
                  </h2>
                  <h2 className={styles.TempMobile}>{temp} °C</h2>
                </>
              ) : (
                <h2>Can't get current temperature in {city}.</h2>
              )}
            </div>

            {greenArrow && <span className={styles.GreenArrow}>&#8593;</span>}
            {redArrow && <span className={styles.RedArrow}>&#8595;</span>}
          </div>
        </Container>
      ) : (
        <Spinner />
      )}
    </Layout>
  );
};

export default Home;

// export default connect((state: AppState) => {
//   return {
//     userReducer: state.userReducer
//   };
// })(Home);

// const mapStateToProps = (state: AppState) => ({
//   myInfo: state.userReducer.user
// });
// export default connect(mapStateToProps)(Home);

// export default connect((state: AppState) => {
//   return {
//     myInfo: state.userReducer
//   };
// })(
//   reduxForm<HomeProps>({ form: 'userInfoForm' })(Home)
// );

// export default Home;
