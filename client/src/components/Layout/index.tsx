import Footer from "../Footer";
import Header from "../Header";
import Main from "../Main";
import React from "react";
import SiteContainer from "../SiteContainer";

const Layout: React.FC = ({ children }) => (
  <SiteContainer>
    <Header />
    <Main>{children}</Main>
    <Footer />
  </SiteContainer>
);

export default Layout;
