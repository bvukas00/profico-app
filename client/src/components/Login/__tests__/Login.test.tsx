import React from 'react';
import { shallow } from 'enzyme';
import Login, { LoginProps } from '../';

describe('Login [component] ->', () => {
  let props: LoginProps;

  beforeEach(() => {
    props = {
      anyTouched: false,
      array: undefined,
      asyncValidate: undefined,
      asyncValidating: undefined,
      autofill: undefined,
      blur: undefined,
      change: undefined,
      clearAsyncError: undefined,
      destroy: undefined,
      dirty: undefined,
      error: undefined,
      form: undefined,
      handleSubmit: jest.fn(),
      initialize: undefined,
      initialized: undefined,
      initialValues: undefined,
      invalid: undefined,
      pristine: undefined,
      reset: jest.fn(),
      submitFailed: undefined,
      submitSucceeded: undefined,
      submitting: undefined,
      touch: undefined,
      untouch: undefined,
      valid: undefined,
      warning: undefined,
      registeredFields: undefined
    };
  });

  const output = shallow(<Login {...props} />);
  it('should render correctly', () => {
    expect(output).toMatchSnapshot();
  });

  it('test reset button', () => {
    const output = shallow(<Login {...props} />);
    const resetButton = output.find('.ButtonRed');

    resetButton.simulate('click');
    expect(props.reset).toBeCalled();
    expect(output).toMatchSnapshot();
  });
});
