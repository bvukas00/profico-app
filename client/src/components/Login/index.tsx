import React, { useState } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import {
  Field,
  reduxForm,
  FormSubmitHandler,
  Form,
  InjectedFormProps,
  WrappedFieldInputProps,
  change,
  blur,
  focus
} from 'redux-form';

import setAuthToken from '../../utils/setAuthToken';
import Validator from '../../utils/validation';
import { Container } from '..';
import { Login as LoginModel, LoginInput, User } from '../../models';
import { loadUser, loadUserList } from '../../actions/user';
import styles from './styles.module.scss';

import { AppState } from '../../store';
import { ThunkDispatch } from 'redux-thunk';
import { AppActions } from '../../actions/types';
import { saveToken } from '../../actions/saveToken';
import { getAllUsers, getUserInfo } from '../../utils/User/user';

// izdvoji u zasebnu komponentu
const renderField = ({ input, meta, ...props }: any) => (
  <div style={{ display: 'flex', flexDirection: 'column' }}>
    <input {...input} {...props} />
    {meta.error && meta.touched && (
      <span style={{ color: 'red', marginTop: '.5rem' }}>{meta.error}</span>
    )}
  </div>
);

export type LoginProps = InjectedFormProps<LoginInput>;
// reduxForm nam daje handleSubmit i jos neke propove
const Login: React.FC<LoginProps> = ({ handleSubmit, reset }) => {
  const [formData, setFormData] = useState<LoginInput>({
    username: '',
    password: ''
  });
  const [auth, setAuth] = useState<string>(sessionStorage.getItem('token'));
  const [error, setError] = useState<boolean>(false);
  const dispatch = useDispatch();

  const { username, password } = formData;

  // const onChange = (e: React.ChangeEvent<HTMLInputElement>) =>
  //   setFormData({ ...formData, [e.target.name]: e.target.value });

  const onSubmit: FormSubmitHandler<LoginInput> = async ({
    username,
    password
  }) => {
    const userCredentials = {
      username,
      password
    };

    try {
      const config = {
        headers: {
          'Content-Type': 'application/json'
        }
      };
      const body = JSON.stringify(userCredentials);
      const res = await axios.post<LoginModel>('/login', body, config);
      const { token, admin } = res.data;
      sessionStorage.setItem('token', token);
      setAuthToken(token);
      setAuth(token);
      if (admin) {
        sessionStorage.setItem('admin', 'admin');
        const userList = await getAllUsers();
        dispatch(loadUserList(userList));
      } else sessionStorage.setItem('admin', '');

      // load user to store
      const user = await getUserInfo();
      dispatch(loadUser(user));
    } catch (err) {
      console.log(err);
      setError(true);
    }
  };

  if (auth) {
    return <Redirect to="/home" />;
  }

  return (
    <Container>
      <h1>Log In:</h1>

      <Form onSubmit={handleSubmit(onSubmit)} className={styles.LoginForm}>
        {error ? (
          <p className={'ErrorInput'}>Wrong username or password</p>
        ) : null}
        <Field
          name="username"
          component={renderField}
          type="text"
          className={'Input'}
          placeholder="Enter username..."
          validate={Validator.required}
        />
        <Field
          name="password"
          component={renderField}
          type="password"
          className={'Input'}
          placeholder="Enter password..."
          validate={Validator.required}
        />

        <button type="submit" className={'Button'}>
          Log in
        </button>
        <button type="button" className={'ButtonRed'} onClick={reset}>
          Reset
        </button>
      </Form>

      <p className={styles.Hint}>
        HINT:
        <br />
        username: <strong>b</strong>
        <br />
        password: <strong>123</strong>
      </p>
    </Container>
  );
};

export default reduxForm<LoginInput>({ form: 'loginForm' })(Login);
/*

const mapDispatchToProps = (dispatch: any) => {
  return {
    saveUserToStore: (user: User) => {
      dispatch(loadUser(user));
    }
  };
};

export default connect(
  mapStateToProp,
  mapDispatchToProps
)(
  reduxForm<LoginInput>({ form: 'loginForm' })(Login)
);


interface LinkStateProps {
  token: string;
}

interface LinkDispatchProps {
  saveTokenToStore: (token: string) => void;
}

const mapStateToProp = (state: AppState): LinkStateProps => {
  return {
    token: state.tokenReducer
  };
};

const mapDispatchToProps = (
  dispatch: ThunkDispatch<any, any, AppActions>
): LinkDispatchProps => {
  return {
    saveTokenToStore: (token: string) => {
      dispatch(saveToken(token));
    }
  };
};
export default connect(mapStateToProp, mapDispatchToProps)(Login);

*/
