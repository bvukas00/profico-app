import React from 'react';
import logo from './logo.jpg';
import { Link } from 'react-router-dom';
import styles from './styles.module.scss';

export default () => (
  <Link to="/home">
    <img className={styles.Logo} src={logo} alt="Logo" />
  </Link>
);
