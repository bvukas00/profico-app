import React from 'react';
import Container from '../Container';
import styles from './styles.module.scss';

export default ({ children }: any) => (
  <main className={styles.Main}>
    <Container>{children}</Container>
  </main>
);
