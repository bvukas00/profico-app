import React from 'react';

import { shallow } from 'enzyme';
import Register, { RegisterProps } from '../';

describe('Register [component] ->', () => {
  let props: RegisterProps;
  beforeEach(() => {
    props = {
      close: jest.fn()
    };
  });
  it('should render correctly', () => {
    const output = shallow(<Register {...props} />);

    expect(output).toMatchSnapshot();
  });
});
