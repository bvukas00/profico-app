import React, { useState } from 'react';
import axios from 'axios';
import { UserInfo, Container } from '..';
import { User } from '../../models';
import styles from './styles.module.scss';

export interface RegisterProps {
  close: () => void;
}

const Register: React.FC<RegisterProps> = ({ close }) => {
  const [formData] = useState<User>({
    name: '',
    email: '',
    username: '',
    city: '',
    role: 'admin'
  });
  const [error, setError] = useState<string>('');
  const [success, setSuccess] = useState<string>('');

  const { name, username, city, email, role } = formData;

  const saveChanges = async (newUser: User) => {
    try {
      const res = await axios.post<string>('/admin/register', newUser);
      setSuccess(res.data);
    } catch (err) {
      console.log(err);
      if (err.response.data.errors) setError(err.response.data.errors[0].msg);
    }
  };

  return (
    <Container className={styles.Container}>
      <h1>Register new user:</h1>
      {error ? <p className={'ErrorInput'}>{error}</p> : null}
      {success ? <p className={styles.SuccessInput}>{success}</p> : null}
      <UserInfo
        name={name}
        username={username}
        city={city}
        email={email}
        role={role}
        handleDelete={close}
        deleteValue={'Close'}
        onSubmit={saveChanges}
        parent={'Register'}
      ></UserInfo>
    </Container>
  );
};

export default Register;
