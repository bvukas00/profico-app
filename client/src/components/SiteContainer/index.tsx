import React from 'react';
import styles from './styles.module.scss';

const SiteContainer: React.FC = ({ children }) => (
  <div className={styles.Container}> {children} </div>
);

export default SiteContainer;
