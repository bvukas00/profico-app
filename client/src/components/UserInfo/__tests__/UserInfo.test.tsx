import React from 'react';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';
import store from '../../../store';
import UserInfo, { UserInfoProps, WithInjectedFormProps } from '../';

describe('Home [component] ->', () => {
  let props: UserInfoProps;
  beforeEach(() => {
    props = {
      name: '',
      username: '',
      city: '',
      email: '',
      role: '',
      deleteValue: '',
      handleDelete: jest.fn(),
      onSubmit: jest.fn()
    };
  });
  const output = shallow(
    <Provider store={store}>
      <UserInfo {...props} />
    </Provider>
  );

  it('should render correctly', () => {
    console.log(output.debug());
    expect(output).toMatchSnapshot();
  });

  it('test delete button', () => {
    const deleteButton = shallow(
      <input
        type="button"
        className={'ButtonRed'}
        value={props.deleteValue}
        onClick={props.handleDelete}
      ></input>
    );

    deleteButton.simulate('click');
    expect(props.handleDelete).toBeCalled();
  });
});
