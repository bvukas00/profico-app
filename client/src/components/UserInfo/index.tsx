import React, { useState, useEffect } from 'react';
import { Field, reduxForm, Form, InjectedFormProps, change } from 'redux-form';
import { connect, useDispatch } from 'react-redux';
import Normalize from '../../utils/normalize';
import Validator from '../../utils/validation';
import { Container } from '..';
import { User } from '../../models';
import { AppState } from '../../store';
import { UserReducerState } from '../../reducers/userReducer';
import styles from './styles.module.scss';

export interface UserInfoProps {
  name: string;
  username: string;
  city: string;
  email: string;
  role: string;
  onSubmit: (user: User) => Promise<void>;
  deleteValue: string;
  handleDelete: () => Promise<void> | void;
  modal?: boolean;
  parent?: string;
  userToUpdate?: User;
}

const renderField = ({ input, meta, ...props }: any) => (
  <div style={{ display: 'flex', flexDirection: 'column' }}>
    <input {...input} {...props} />
    {meta.error && meta.touched && (
      <span style={{ color: 'red', marginTop: '.5rem' }}>{meta.error}</span>
    )}
  </div>
);

export type WithInjectedFormProps = InjectedFormProps<User, UserInfoProps> &
  UserInfoProps;

const UserInfo: React.FC<WithInjectedFormProps> = props => {
  const { handleSubmit, onSubmit } = props;
  const isCancelled = React.useRef<boolean>(false);
  const [formData, setFormData] = useState<User>({
    name: '',
    username: '',
    password: '',
    city: '',
    email: '',
    role: 'client'
  });
  const { name, username, city, email, role } = props;
  useEffect(() => {
    // const { initialize } = props
    // initialize({ name, username, city, email, role });

    // const { parent } = props;
    // console.log('UserInfo');
    // console.log(props.initialValues);
    // console.log('parent ' + parent);
    // console.log(props.userToUpdate);

    // if (parent == 'Home') {
    //   const { name, username, city, email, role } = props;

    //   setFormData({ name, username, password: '', city, email, role });
    //   console.log(' Home');
    //   console.log(formData);
    // } else {
    //   setFormData({
    //     name: '',
    //     username: '',
    //     password: '',
    //     city: '',
    //     email: '',
    //     role: ''
    //   });
    //   console.log('Nije Home');
    //   console.log(formData);
    // }

    // setFormData({ name, username, password: '', city, email, role });
    return () => {
      isCancelled.current = true;
    };
  }, [username]);

  // const onChange = (
  //   e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  // ) => setFormData({ ...formData, [e.target.name]: e.target.value });

  const onChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const { change } = props;
    change('role', e.target.value);
  };

  /*
  const onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    props.handleSubmit(formData);
  };
*/
  return (
    <Container className={props.modal && styles.ModalContent}>
      <Form onSubmit={handleSubmit(onSubmit)} className={styles.LoginForm}>
        <div className={styles.InputDiv}>
          Name:<br></br>
          <Field
            name="name"
            component={renderField}
            autoFocus
            type="text"
            className={'Input'}
            validate={Validator.required}
          />
        </div>
        <div className={styles.InputDiv}>
          Email:<br></br>
          <Field
            name="email"
            component={renderField}
            type="text"
            className={'Input'}
            validate={Validator.required}
          />
        </div>
        <div className={styles.InputDiv}>
          Username: <br></br>
          <Field
            name="username"
            component={renderField}
            normalize={Normalize.lower}
            type="text"
            className={'Input'}
            onChange={onChange}
            validate={Validator.required}
          />
        </div>
        <div className={styles.InputDiv}>
          Password: <br></br>
          <Field
            name="password"
            component={renderField}
            type="text"
            className="Input"
            placeholder=""
            validate={Validator.minLength}
          />
        </div>
        <div className={styles.InputDiv}>
          City: <br></br>
          <Field
            name="city"
            component={renderField}
            type="text"
            className={'Input'}
            validate={Validator.required}
          />
        </div>
        {role === 'admin' && (
          <div className={styles.InputDiv}>
            Status: <br></br>
            <select name="role" className={'Input'} onChange={onChange}>
              <option value="admin">Admin</option>
              <option value="client">Client</option>
            </select>
          </div>
        )}

        <input
          type="button"
          className={'ButtonRed'}
          value={props.deleteValue}
          onClick={props.handleDelete}
        ></input>
        <input type="submit" className={'Button'} value={'Save'} />
      </Form>
    </Container>
  );
};

export default connect(
  (state: AppState) => {
    return {
      initialValues: state.userReducer.user,
      userToUpdate: state.userReducer.updateUser
    };
  },
  { change }
)(
  reduxForm<User, UserInfoProps>({
    form: 'userInfoForm',
    enableReinitialize: true
  })(UserInfo)
);
