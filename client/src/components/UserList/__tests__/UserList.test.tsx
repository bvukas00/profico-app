import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { shallow } from 'enzyme';
import UserList from '../';

describe('UserList [component] ->', () => {
  const output = shallow(
    <Router>
      <UserList />
    </Router>
  );

  it('should render correctly', () => {
    expect(output).toMatchSnapshot();
  });

  it('test Add new user button click', () => {
    const openRegister = jest.fn();
    // event handler
    // const button = output.find('button');
    const button = shallow(
      <button className={'Button'} onClick={openRegister}>
        Add new user
      </button>
    );

    button.simulate('click');
    expect(openRegister).toBeCalled();
    expect(output).toMatchSnapshot();
    console.log(output.debug());
  });

  it('test Home button click', () => {
    const returnHome = jest.fn();
    const button = shallow(
      <button className={'Button'} onClick={returnHome}>
        Home
      </button>
    );

    button.simulate('click');
    expect(returnHome).toBeCalled();
    expect(output).toMatchSnapshot();
  });
});
