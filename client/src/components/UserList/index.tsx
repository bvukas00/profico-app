import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Layout } from '..';
import Register from '../Register';
import UserTable from '../UserTable';
import styles from './styles.module.scss';

const UserList = () => {
  const [register, setRegister] = useState<boolean>(false);

  let history = useHistory();

  const closeRegister = () => setRegister(false);

  const openRegister = () => setRegister(true);

  const returnHome = () => history.push('/home');

  return (
    <Layout>
      <div className={styles.ButtonsTop}>
        {/* napravi button komponentu */}
        <button className={'Button'} onClick={returnHome}>
          Home
        </button>
        <button className={'Button'} onClick={openRegister}>
          Add new user
        </button>
      </div>
      {register && <Register close={closeRegister} />}
      <h1>List of users:</h1>
      <UserTable />
    </Layout>
  );
};

export default UserList;
