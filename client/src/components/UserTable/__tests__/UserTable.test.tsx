import React from 'react';
import { Provider } from 'react-redux';
import { shallow } from 'enzyme';
import UserTable, { UserTableProps } from '../';
import store from '../../../store';

describe('Home [component] ->', () => {
  let props: UserTableProps;
  const output = shallow(
    <Provider store={store}>
      <UserTable />
    </Provider>
  );
  beforeEach(() => {
    props = {
      myUsername: 'username',
      userList: []
    };
  });

  it('should render correctly', () => {
    expect(output).toMatchSnapshot();
  });
});
