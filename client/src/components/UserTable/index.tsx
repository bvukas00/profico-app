import React, { useState, useEffect } from 'react';
import axios from 'axios';
// kod importanja prvo react, pa ove npm stvari, pa costum componente i na kraju style
import { Container, UserInfo } from '..';
import { User, UserTable as UserTableModel } from '../../models';

import styles from './styles.module.scss';
import { AppState } from '../../store';
import { connect, useDispatch } from 'react-redux';
import { updateUser } from '../../actions/user';

export interface UserTableProps {
  myUsername: string;
  userList: Array<User>;
}

const UserTable: React.FC<UserTableProps> = ({ myUsername, userList }) => {
  // const [data, setData] = useState<UserTableModel>({
  //   fetched: false,
  //   users: [],
  //   myUsername: ''
  // });
  const [edit, setEdit] = useState<boolean>(false);
  const [userToUpdate, setUserToUpdate] = useState<User>({
    name: '',
    username: '',
    city: '',
    email: '',
    role: ''
  });
  const dispatch = useDispatch();

  // useEffect(() => {
  //   const fetchData = async () => {
  //     // try {
  //     //   const allUsers = await axios.get<Array<User>>('/admin/allUsers');
  //     //   const username = await axios.get<string>('/admin/myUsername');
  //     //   console.log(myUsername2);
  //     //   console.log(userList);
  //     //   setData({
  //     //     fetched: true,
  //     //     users: allUsers.data,
  //     //     myUsername: username.data
  //     //   });
  //     // } catch (err) {
  //     //   console.log(err);
  //     // }
  //   };
  //   fetchData();
  // }, []);

  const closeModal = () => {
    setEdit(false);
  };

  const deleteUser = async (user: User) => {
    if (
      window.confirm(
        `This action can not be undone! Do you want to delete ${user.username}?`
      )
    ) {
      const body = {
        username: user.username,
        city: user.city
      };
      try {
        const res = await axios.post<string>('admin/deleteByUsername', body);
        console.log(res.data);
      } catch (err) {
        console.log(err);
      }
    }
  };

  const handleEdit = ({ name, username, city, email, role }: User) => {
    setUserToUpdate({ name, username, city, email, role });
    // dispatch(updateUser({ name, username, city, email, role, password: '' }));
    setEdit(true);
  };

  const saveChanges = async (updatedUser: User) => {
    const body = {
      ...updatedUser,
      oldUsername: userToUpdate.username,
      oldCity: userToUpdate.city
    };

    try {
      const res = await axios.post<User>('/admin/update', body);
      console.log(res);
      alert('Changes saved successfully!');
      closeModal();
    } catch (err) {
      console.log(err);
      if (err.response.data.errors) alert(err.response.data.errors[0].msg);
    }
  };

  const { name, username, city, email } = userToUpdate;
  console.log(userToUpdate);
  // const { fetched, users, myUsername } = data;

  return (
    <Container>
      <div className={edit ? styles.ModalBlock : styles.ModalNone}>
        <UserInfo
          name={name}
          username={username}
          city={city}
          email={email}
          role={'admin'}
          handleDelete={closeModal}
          deleteValue={'Close'}
          onSubmit={saveChanges}
          modal={true}
        ></UserInfo>
      </div>
      <table className={styles.UsersTable}>
        <thead className={styles.TableHead}>
          <tr>
            <th className={styles.TableCell}>Username</th>
            <th className={styles.TableCell}>Name</th>
            <th className={styles.TableCell}>Email</th>
            <th className={styles.TableCell}>City</th>
            <th className={styles.TableCell}>Status</th>
            <th className={styles.TableCell}>Edit</th>
            <th className={styles.TableCell}>Delete</th>
          </tr>
        </thead>
        <tbody>
          {userList.map((user: User) => {
            const edit = () => {
              handleEdit(user);
            };
            const deleteSelectedUser = () => {
              deleteUser(user);
            };

            return (
              <tr className={styles.TableRow} key={user.username}>
                <td className={styles.TableCell}>{user.username}</td>
                <td className={styles.TableCell}>{user.name}</td>
                <td className={styles.TableCell}>{user.email}</td>
                <td className={styles.TableCell}>{user.city}</td>
                <td className={styles.TableCell}>{user.role}</td>
                {user.role !== 'admin' || user.username === myUsername ? (
                  <>
                    <td>
                      <button className={'Button'} onClick={edit}>
                        Edit
                      </button>
                    </td>
                    <td>
                      <button
                        className={'ButtonRed'}
                        onClick={deleteSelectedUser}
                      >
                        Delete
                      </button>
                    </td>
                  </>
                ) : (
                  <>
                    <td className={styles.TableCell}></td>
                    <td className={styles.TableCell}></td>
                  </>
                )}
              </tr>
            );
          })}
        </tbody>
      </table>
    </Container>
  );
};

const mapStateToProps = (state: AppState) => ({
  myUsername: state.userReducer.user.username,
  userList: state.userReducer.userList
});
export default connect(mapStateToProps)(UserTable);
