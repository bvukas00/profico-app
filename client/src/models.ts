export interface User {
  name: string;
  username: string;
  email: string;
  city: string;
  role: string;
  password?: string;
  _id?: string;
}

export interface Login {
  token: string;
  admin: boolean;
}

export interface LoginInput {
  username: string;
  password: string;
}

export interface UserTable {
  fetched: boolean;
  users: Array<User>;
  myUsername: string;
}
