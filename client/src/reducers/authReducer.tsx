import { IS_AUTHENTICATED } from '../actions/types';
import { Reducer } from 'redux';

export interface AuthReducerState {
  isAuthenticated: boolean;
}

export const initialState: AuthReducerState = {
  isAuthenticated: false
};

const authReducer: Reducer<AuthReducerState> = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case IS_AUTHENTICATED:
      return {
        ...state,
        isAuthenticated: payload
      };
    default:
      return state;
  }
};

export default authReducer;
