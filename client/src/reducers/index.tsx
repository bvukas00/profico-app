import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import tokenReducer from './tokenReducer';
import userReducer from './userReducer';
import authReducer from './authReducer';

const rootReducer = combineReducers({
  form: formReducer,
  tokenReducer,
  userReducer,
  authReducer
});

export default rootReducer;
