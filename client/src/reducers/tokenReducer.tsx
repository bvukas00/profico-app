import { SAVE_TOKEN, TokenActionTypes } from "../actions/types";

const initialState: string = "";

const tokenReducer = (state = initialState, action: TokenActionTypes) => {
  const { type, payload } = action;

  switch (type) {
    case SAVE_TOKEN:
      return action.payload;
    default:
      return state;
  }
};

export default tokenReducer;
