import {
  LOAD_USER,
  LOAD_USER_LIST,
  UPDATE_USER,
  UserActionTypes
} from '../actions/types';
import { User } from '../models';
import { Reducer } from 'redux';

export interface UserReducerState {
  user: User;
  userList: Array<User>;
  updateUser: User;
}

const initialState: UserReducerState = {
  user: {
    name: 'ddd',
    username: '',
    email: '',
    city: '',
    role: ''
  },
  userList: [],
  updateUser: {
    name: '',
    username: '',
    email: '',
    city: '',
    role: '',
    password: ''
  }
};

const userReducer: Reducer<UserReducerState> = (
  state = initialState,
  { type, payload }
) => {
  switch (type) {
    case LOAD_USER:
      return {
        ...state,
        user: payload
      };
    case LOAD_USER_LIST:
      return {
        ...state,
        userList: payload
      };
    case UPDATE_USER:
      return {
        ...state,
        updateUser: payload
      };
    default:
      return state;
  }
};

export default userReducer;
