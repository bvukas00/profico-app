import { RouteComponentProps } from 'react-router-dom';
import { Dispatch } from 'redux';

import { initialState, AuthReducerState } from '../../../reducers/authReducer';

export interface AuthContextValues extends AuthReducerState {
  refreshData: () => Promise<void>;
}

export const initialValues: AuthContextValues = {
  ...initialState,
  refreshData: () => new Promise(() => {})
};

export interface AuthProviderProps extends RouteComponentProps {
  dispatch: Dispatch;
  auth: AuthReducerState;
}
