import React, { Component } from 'react';

import { withRouter } from 'react-router-dom';
import { compose } from 'redux';
import { connect, batch } from 'react-redux';

import {
  initialValues,
  AuthProviderProps,
  AuthContextValues
} from './AuthContext.helpers';
import { AppState } from '../../../store';
import { setIsAuthenticated } from '../../../actions/auth';
import { User } from '../../../models';
import { loadUser, loadUserList } from '../../../actions/user';
import { getUserInfo, getAllUsers } from '../../User/user';
import setAuthToken from '../../setAuthToken';

const AuthContext = React.createContext<AuthContextValues>(initialValues);

export default AuthContext;
export const AuthConsumer = AuthContext.Consumer;
export const useAuth = () => React.useContext(AuthContext);

class AuthProviderBase extends Component<AuthProviderProps> {
  public async componentDidMount() {
    const { dispatch } = this.props;

    if (sessionStorage.getItem('token')) {
      dispatch(setIsAuthenticated(true));
      await this.refreshData();
    } else {
      dispatch(setIsAuthenticated(false));
    }
  }

  refreshData = async () => {
    const { dispatch } = this.props;
    const token = sessionStorage.getItem('token');
    const admin = sessionStorage.getItem('admin');

    setAuthToken(token);

    try {
      const user = await getUserInfo();
      dispatch(loadUser(user));
      if (admin) {
        const userList = await getAllUsers();
        dispatch(loadUserList(userList));
      }
    } catch (err) {
      console.log(err);
    }
  };

  public render() {
    const { children, auth } = this.props;

    return (
      <AuthContext.Provider value={{ ...auth, refreshData: this.refreshData }}>
        {children}
      </AuthContext.Provider>
    );
  }
}

const composed = compose(
  withRouter,
  connect((state: AppState) => ({
    auth: state.authReducer,
    user: state.userReducer
  }))
);

export const AuthProvider = composed(AuthProviderBase) as React.ComponentType;
