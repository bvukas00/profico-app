import axios from 'axios';

import { User } from '../../models';

export const getUserInfo = async () => {
  const userInfo = await axios.get<User>('user/userinfo');
  return userInfo.data;
};

export const getAllUsers = async () => {
  const allUsers = await axios.get<Array<User>>('/admin/allUsers');
  const userList = allUsers.data.map(user => ({
    name: user.name,
    username: user.username,
    email: user.email,
    city: user.city,
    role: user.role
  }));
  return userList;
};
