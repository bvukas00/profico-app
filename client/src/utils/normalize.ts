export default class Normalize {
  static lower = (value: string) => value && value.toLowerCase();
}
