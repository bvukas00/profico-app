class Validation {
  static isRequired = (value?: string) => !value;
  static isLength3 = (value?: string) => {
    if (value && value.length >= 3) {
      return true;
    } else return false;
  };
}

export default class Validator {
  static required = (value: string) =>
    Validation.isRequired(value) ? "Required" : undefined;
  static minLength = (value: string) =>
    Validation.isLength3(value)
      ? undefined
      : "Minimum password length is 3 characters";
}
