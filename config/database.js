const mongoose = require("mongoose");
const createInitialUserAndCity = require("../utils/createInitalUserAndCity");

require("dotenv").config();

const connectDatabase = async () => {
  try {
    await mongoose.connect(process.env.MONGO_URI, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true
    });
    console.log("Database connected...");

    // if the app starts first time, create inital user and city
    let usersCollectionExist = await mongoose.connection.db
      .collection("users")
      .countDocuments();
    if (!usersCollectionExist) {
      createInitialUserAndCity();
    }
  } catch (err) {
    console.error("Error in config: " + err.message);
    process.exit(1);
  }
};

module.exports = connectDatabase;
