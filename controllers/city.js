const axios = require("axios");
const City = require("../models/City");
const User = require("../models/User");

require("dotenv").config();

const cityController = {
  deleteCityIfNotNeeded: async city => {
    try {
      console.log(city);
      const user = await User.findOne({ city }, err => {
        if (err) console.log(err);
      });
      if (!user) {
        City.deleteOne({ name: city }, err => {
          if (err) console.log(err);
          console.log("Successfuly deleted city " + city);
        });
      }
    } catch (err) {
      console.log("deleteCityIfNotNeeded: " + err);
    }
  },

  findByNameAndUpdateTemperature: async (name, temperature) => {
    return await City.findOneAndUpdate({ name }, { temperature });
  },

  getAll: () => {
    return City.find({});
  },

  getAllWeatherIds: () => {
    return City.find({}, { weatherId: 1 });
  },

  getTemperature: async (req, res) => {
    try {
      let city = await City.findOne({ name: req.params.city }, err => {
        if (err) console.log(err);
      }).select("temperature");
      console.log(city);
      if (city) res.send(city.temperature);
      else res.status(400).send("City not found in database");
    } catch (err) {
      console.log(err);
    }
  },

  getTemperatureByCityName: async city => {
    try {
      let weather = await axios.get(
        `http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${process.env.WEATHER_API_KEY}&units=metric`
      );
      const temperature = weather.data.main.temp.toString();
      // if city is not found returns undefined, else returns temperature in celzius (string)
      return temperature;
    } catch (err) {
      console.error(err.message);
    }
  },

  saveCityIfNotExist: async name => {
    try {
      let city = await City.findOne({ name });

      // if city is not in database already
      if (!city) {
        let temperature = await cityController.getTemperatureByCityName(name);
        city = new City({
          name,
          temperature
        });
        // if city is found succesfully temperature will contain number
        // if city is not found temperature will be undefined
        if (temperature) await city.save();
      }
    } catch (err) {
      console.error("saveCityIfNotExist: " + err.message);
    }
  }
};

module.exports = cityController;
