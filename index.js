const connectDatabase = require("./config/database");
const events = require("events");
const express = require("express");
const http = require("http");
const auth = require("./middleware/auth");
const isAdmin = require("./middleware/isAdmin");
const path = require("path");
const socketIo = require("socket.io");
const swaggerJSDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const getTempAndEmitIfChanged = require("./utils/getTempAndEmitIfChanged");
const updateCityTemperature = require("./utils/updateCityTemp");

require("dotenv").config();

const eventEmitter = new events.EventEmitter();
const PORT = process.env.PORT || 5000;
const app = express();

connectDatabase();

// connect ExpressJS server and Socket.IO
const server = http.createServer(app);
const io = socketIo(server);

const swaggerDefinition = {
  info: {
    title: "Weather API",
    version: "1.0.0"
  },
  servers: ["localhost:5000", "https://ancient-temple-80282.herokuapp.com/"],
  basePath: "/",
  securityDefinitions: {
    bearerAuth: {
      type: "apiKey",
      name: "x-auth-token",
      scheme: "bearer",
      in: "header"
    }
  }
};
const options = {
  swaggerDefinition,
  apis: ["./routes/*.js"]
};
const swaggerSpec = swaggerJSDoc(options);

app.get("/swagger.json", (req, res) => {
  res.setHeader("Content-Type", "application/json");
  res.send(swaggerSpec);
});

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));
// Bodyparser middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
// Routes
app.use("/admin", auth, isAdmin, require("./routes/admin"));
app.use("/city", require("./routes/city"));
app.use("/login", require("./routes/login"));
app.use("/user", auth, require("./routes/user"));

// Serve static assets in production
if (process.env.NODE_ENV === "production") {
  // Set static folder
  app.use(express.static("client/build"));

  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

// update temp of each city in database every 10 minutes
updateCityTemperature(eventEmitter);
setInterval(() => updateCityTemperature(eventEmitter), 600000);

// update temp of connected users
io.on("connection", socket => {
  let { city, temp } = socket.handshake.query;
  console.log("New client connected");
  // when temperatures in city collection update
  eventEmitter.addListener("connection", () =>
    getTempAndEmitIfChanged(socket, city, temp)
  );
  socket.on("disconnect", () => {
    console.log("Client disconnected");
  });
});

server.listen(PORT, () =>
  console.log(`Server listening on the port ${PORT}...`)
);

module.exports = app;
