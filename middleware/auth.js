const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  // Get token from header
  const token = req.header("x-auth-token");

  // Check if not token
  if (!token) {
    return res.status(401).send("No token, access denied");
  }

  // Verify token
  try {
    const decoded = jwt.verify(token, process.env.JWT_SECRET);

    req.decodedUser = decoded;

    next();
  } catch (err) {
    res.status(401).send("Token is not valid");
  }
};
