module.exports = (req, res, next) => {
  if (req.decodedUser.role == "admin") {
    next();
  } else res.status(403).send("Access denied");
};
