const mongoose = require("mongoose");

/**
 * @swagger
 * definitions:
 *   City:
 *     type: object
 *     properties:
 *       _id:
 *         type: integer
 *       name:
 *         type: string
 *       temperature:
 *         type: string
 *       required:
 *         - name
 *         - temperature
 */
const CitySchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true,
    lowercase: true
  },
  temperature: {
    type: String,
    required: true
  }
});

module.exports = City = mongoose.model("city", CitySchema);
