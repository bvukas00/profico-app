const mongoose = require("mongoose");

/**
 * @swagger
 * definitions:
 *   User:
 *     type: object
 *     properties:
 *       _id:
 *         type: integer
 *       name:
 *         type: string
 *       username:
 *         type: string
 *       email:
 *         type: string
 *       password:
 *         type: string
 *         format: password
 *       city:
 *         type: string
 *       role:
 *         type: string
 *       required:
 *         - username
 *         - email
 *         - password
 *         - city
 *         - role
 */
const UserSchema = new mongoose.Schema({
  name: {
    type: String
  },
  username: {
    type: String,
    required: true,
    unique: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true,
    lowercase: true
  },
  role: {
    type: String,
    required: true,
    lowercase: true
  }
});

module.exports = User = mongoose.model("user", UserSchema);
