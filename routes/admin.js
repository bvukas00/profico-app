const adminController = require("../controllers/admin");
const express = require("express");
const { check } = require("express-validator");
const router = express.Router();

/**
 * @swagger
 * /admin/allUsers:
 *   get:
 *     tags:
 *       - admin
 *     name: Get all users
 *     summary: Get all users from datablase (all fields except password)
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: All users fetched
 *       '401':
 *         description: Token is not valid
 *       '403':
 *         description: User is not admin
 *       '500':
 *         description: Server error while fetching users
 */
router.get("/allUsers", adminController.getAllUsers);

/**
 * @swagger
 * /admin/myUsername:
 *   get:
 *     tags:
 *       - admin
 *     name: Get admin username
 *     summary: Get username of logged in admin
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: Username fetched
 *       '401':
 *         description: Token is not valid
 *       '403':
 *         description: User is not admin
 *       '500':
 *         description: Server error in adminController.getMyUsername
 */
router.get("/myUsername", adminController.getMyUsername);

/**
 * @swagger
 * /admin/deleteByUsername:
 *   post:
 *     tags:
 *       - admin
 *     name: Admin deletes user
 *     summary: Admin delete another user or himself
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             username:
 *               type: string
 *             city:
 *               type: string
 *         required:
 *           - username
 *           - city
 *     responses:
 *       '200':
 *         description: User found by username and deleted from db. His city is deleted from db if no other user need temperature from that city.
 *       '401':
 *         description: Token is not valid
 *       '403':
 *         description: User is not admin / user is trying to delete another admin
 *       '404':
 *         description: User with sent username does not exist in database
 *       '500':
 *         description: Server error while deleting user
 */
router.post("/deleteByUsername", adminController.deleteByUsername);

/**
 * @swagger
 * /admin/register:
 *   post:
 *     tags:
 *       - admin
 *     name: Admin register user
 *     summary: Admin register new user
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *             email:
 *               type: string
 *             username:
 *               type: string
 *             password:
 *               type: string
 *             city:
 *               type: string
 *             role:
 *               type: string
 *         required:
 *           - email
 *           - username
 *           - password
 *           - city
 *           - role
 *     responses:
 *       '200':
 *         description: User is added to database
 *       '400':
 *         description: Some information that user submitted is not acceptable
 *       '401':
 *         description: Token is not valid
 *       '403':
 *         description: User is not admin / Username already taken
 *       '500':
 *         description: Server error while registring user
 */
router.post(
  "/register",
  [
    check("username", "Username is required")
      .not()
      .isEmpty(),
    check("email", "Please include a valid email").isEmail(),
    check(
      "password",
      "Please enter a password with 3 or more characters"
    ).isLength({ min: 3 }),
    check("city", "City name is required")
      .not()
      .isEmpty()
  ],
  adminController.registerUser
);

/**
 * @swagger
 * /admin/update:
 *   post:
 *     tags:
 *       - admin
 *     name: Update another user's info
 *     summary: Admin updates another user's info
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *             email:
 *               type: string
 *             username:
 *               type: string
 *             password:
 *               type: string
 *             city:
 *               type: string
 *             role:
 *               type: string
 *             oldUsername:
 *               type: string
 *             oldCity:
 *               type: string
 *         required:
 *           - email
 *           - username
 *           - password
 *           - city
 *           - role
 *           - oldUsername
 *           - oldCity
 *     responses:
 *       '200':
 *         description: User is updated, new city is added to cities collection and old city is deleted from collection if not used by another user
 *       '400':
 *         description: Some information that user submitted is not acceptable
 *       '401':
 *         description: Token is not valid, access denied
 *       '403':
 *         description: User is not admin / user is trying to delete another admin / Username already taken
 *       '500':
 *         description: Server error while registring user
 */
router.post(
  "/update",
  [
    check("username", "Username is required")
      .not()
      .isEmpty(),
    check("email", "Please include a valid email").isEmail(),
    check(
      "password",
      "Please enter a password with 3 or more characters"
    ).isLength({ min: 3 }),
    check("city", "City name is required")
      .not()
      .isEmpty()
  ],
  adminController.updateUser
);

module.exports = router;
