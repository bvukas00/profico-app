const cityController = require("../controllers/city");
const express = require("express");

const router = express.Router();

/**
 * @swagger
 * /city/temperature/{city}:
 *   get:
 *     tags:
 *       - cities
 *     name: City Temperature
 *     summary: Get temperature of the city
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: city
 *         in: path
 *         schema:
 *           type: string
 *         required: true
 *         description: Name of the city to get temperature
 *     responses:
 *       '200':
 *         description: City is found and current temperature is returned
 *       '400':
 *         description: City is not found in database
 */
router.get("/temperature/:city", cityController.getTemperature);

module.exports = router;
