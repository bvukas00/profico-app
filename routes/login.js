const loginController = require("../controllers/login");
const express = require("express");
const { check } = require("express-validator");

const router = express.Router();

/**
 * @swagger
 * /login:
 *   post:
 *     tags:
 *       - login
 *     name: Login
 *     summary: Logs in a user
 *     produces:
 *       - application/json
 *     consumes:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             username:
 *               type: string
 *             password:
 *               type: string
 *               format: password
 *         required:
 *           - username
 *           - password
 *     responses:
 *       '200':
 *         description: User found and logged in successfully
 *       '400':
 *         description: Wrong username or password
 *       '500':
 *         description: Server error on login
 */
router.post(
  "/",
  [
    check("username", "Wrong username or password").exists(),
    check("password", "Wrong username or password").exists()
  ],
  loginController
);

module.exports = router;
