const userController = require("../controllers/user");
const express = require("express");
const { check } = require("express-validator");
const router = express.Router();

/**
 * @swagger
 * /user/delete:
 *   delete:
 *     tags:
 *       - users
 *     name: Delete self
 *     summary: Delete logged in user
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: User deleted from database
 *       '401':
 *         description: Token is not valid
 *       '500':
 *         description: Erorr while deleting the user
 */
router.delete("/delete", userController.deleteUser);

/**
 * @swagger
 * /user/userInfo:
 *   get:
 *     tags:
 *       - users
 *     name: Get user info
 *     summary: Get information about logged in user
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     responses:
 *       '200':
 *         description: Returns logged user object without password and id fields
 *       '401':
 *         description: Token is not valid
 *       '500':
 *         description: Erorr in userController.getUserInfo
 */
router.get("/userInfo", userController.getUserInfo);

/**
 * @swagger
 * /user/update:
 *   post:
 *     tags:
 *       - users
 *     name: Update self
 *     summary: Update information about logged in user
 *     security:
 *       - bearerAuth: []
 *     consumes:
 *       - application/json
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: body
 *         in: body
 *         schema:
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *             email:
 *               type: string
 *             username:
 *               type: string
 *             password:
 *               type: string
 *             city:
 *               type: string
 *             role:
 *               type: string
 *             oldUsername:
 *               type: string
 *             olCity:
 *               type: string
 *         required:
 *           - email
 *           - username
 *           - password
 *           - city
 *           - role
 *           - oldUsername
 *           - olCity
 *     responses:
 *       '200':
 *         description: User information updated successfully
 *       '400':
 *         description: Some information that user submitted is not acceptable
 *       '401':
 *         description: Token is not valid
 *       '500':
 *         description: Server error while updating user
 */
router.post(
  "/update",
  [
    check("username", "Username is required")
      .not()
      .isEmpty(),
    check("email", "Please include a valid email").isEmail(),
    check(
      "password",
      "Please enter a password with 3 or more characters"
    ).isLength({ min: 3 }),
    check("city", "City name is required")
      .not()
      .isEmpty()
  ],
  userController.updateInfo
);

module.exports = router;
