const bcrypt = require("bcryptjs");
const cityController = require("../controllers/city");
const City = require("../models/City");
const User = require("../models/User");

const createInitalUserAndCity = async () => {
  let password = "123";
  const salt = await bcrypt.genSalt(10);
  password = await bcrypt.hash(password, salt);

  const cityName = "havana";
  const temperature = cityController.getTemperatureByCityName(cityName);

  user = new User({
    name: "Bruno",
    username: "b",
    email: "bruno@mail.com",
    password,
    city: cityName,
    role: "admin"
  });

  city = new City({
    name: cityName,
    temperature
  });

  User.create(user);
  City.create(city);
};

module.exports = createInitalUserAndCity;
