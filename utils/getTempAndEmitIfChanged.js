const City = require('../models/City');

const getTempAndEmitIfChanged = async (socket, city, tempOnClient) => {
  try {
    let cityTemperature = await City.findOne({ name: city }, err => {
      if (err) console.log(err);
    }).select('temperature');

    let { temperature } = cityTemperature;
    if (tempOnClient !== temperature) {
      socket.emit('FromAPI', temperature); // Emitting a new message. It will be consumed by the client
    }
  } catch (error) {
    console.error(`getTempAndEmitIfChanged: ${error}`);
  }
};

module.exports = getTempAndEmitIfChanged;
