const axios = require("axios");
const cityController = require("../controllers/city");

const updateCityTemperature = async eventEmitter => {
  try {
    let cities = await cityController.getAll();

    for (let city of cities) {
      getTempAndUpdate(city);
    }

    eventEmitter.emit("connection");
  } catch (err) {
    console.error(err.message);
  }
};

const getTempAndUpdate = async city => {
  try {
    let { name, temperature } = city;
    // console.log("inital " + name + " " + temperature);
    // get temperature
    let weather = await axios.get(
      `http://api.openweathermap.org/data/2.5/weather?q=${name}&units=metric&appid=${process.env.WEATHER_API_KEY}`
    );
    let newTemp = weather.data.main.temp;
    newTemp = newTemp.toString();
    // console.log("updated  " + name + " " + newTemp)

    // update DB if necessary
    if (temperature !== newTemp) {
      cityController.findByNameAndUpdateTemperature(name, newTemp);
      // console.log("temp is updated in DB " + name.toUpperCase());
    }
  } catch (err) {
    console.log(err);
  }
};

module.exports = updateCityTemperature;
